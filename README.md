Conservadroid-x86 Project
=========================

What's this?
------------
This project publish ISO images based on Android-x86. All component is from open git repositories.

ISO images are legal clean?
---------------------------
All git repositories are licensed under free.
Some repository may be under opensource license.
Some may be "free-beer" license.
Anyway we exclude binaries that require NDA/commercial like licenses.

But we can't know if each component breaks intellectual property rights.
This issue is not only to us but to all opensources.
So we can't guarantee. Just we can say "we are always care about this issue." 

This project is against for Android-x86?
----------------------------------------
No. This project is a kind of testbed.
Works will be contributed to Android-x86 project.

I found binaries that are not licensed under free.
--------------------------------------------------
Please report our issue track. We will check and remove them if they are not unsuitable.

Who's maintainer?
-----------------
Masaki Muranaka (monaka at monami-ya.jp, monaka at android-x86.org)
